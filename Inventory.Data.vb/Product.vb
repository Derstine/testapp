﻿Imports System.Data.OleDb
Imports Dapper
Imports Inventory.Component
Imports Inventory.Model

Public Class Product : Implements  IProduct
    


    Public Async Function IProduct_GetAllProduct() As Task(Of List(Of ProductModel)) Implements IProduct.GetAllProduct
        Dim sql As String = "Select * From Product"

        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return (Await con.QueryAsync(Of ProductModel)(sql)).ToList()
        End Using
    End Function

    Public Async Function IProduct_RemoveProduct(id As Integer) As Task(Of Integer) Implements IProduct.RemoveProduct
        Dim sql As String = "Delete from Product where Id=@Id"

        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return (Await con.ExecuteAsync(sql, New With {Key.Id = id}))
        End Using
    End Function

    Public Async Function IProduct_AddNewProduct(product As ProductModel) As Task(Of Integer) Implements IProduct.AddNewProduct
        Dim sql As String = "INSERT INTO Product
                           (
                            Barcode,Category,Price,ProductName,Vendor               
                           )
                           VALUES
                           (
                            @Barcode,@Category,@Price,@ProductName,@Vendor   
                           );"

        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return (Await con.ExecuteAsync(sql, product))
        End Using
    End Function

    Public Async Function IProduct_UpdateProduct(product As ProductModel) As Task(Of Integer) Implements IProduct.UpdateProduct
        Dim sql As String = "Update Product
                           SET
                            Barcode=?Barcode?,
                            Category=?Category?,
                            Price=?Price?,
                            ProductName=?ProductName?,                          
                            Vendor=?Vendor?
                        where ID=?ID?;"

        Using con As IDbConnection = New OleDbConnection(New Connection().BuildConnectionString)
            Return (Await con.ExecuteAsync(sql, product))
        End Using
    End Function
End Class
