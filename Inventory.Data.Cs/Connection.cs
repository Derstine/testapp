﻿using System;
using System.Data.OleDb;
using System.IO;
using System.Net.Mime;

namespace Inventory.Data.Cs
{
    public class Connection
    {
        public string BuildConnectionString
        {
            get
            {
                OleDbConnectionStringBuilder builder = new OleDbConnectionStringBuilder
                {
                    ConnectionString =
                        $@"Data Source={Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Inventory.mdb")}"
                };
                builder.Add("Provider", "Microsoft.Jet.Oledb.4.0");
                return builder.ToString();
            }
        }
    }
}