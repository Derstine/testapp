﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Dapper;
using Inventory.Component;
using Inventory.Model;


namespace Inventory.Data.Cs
{
    public class Product: IProduct
    {
        public async Task<List<ProductModel>> GetAllProduct()
        {
            string sql = "Select * From Product";
            using (IDbConnection con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                return (await con.QueryAsync<ProductModel>(sql)).ToList();
            }
        }

        public async Task<int> AddNewProduct(ProductModel product)
        {
            string sql = @"INSERT INTO Product
                           (
                            Barcode,Category,Price,ProductName,Vendor               
                           )
                           VALUES
                           (
                            @Barcode,@Category,@Price,@ProductName,@Vendor   
                           );";
            using (IDbConnection con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                return (await con.ExecuteAsync(sql, product));
            }
        }

        public async Task<int> UpdateProduct(ProductModel product)
        {
            string sql = @"Update Product
                           SET
                            Barcode=?Barcode?,
                            Category=?Category?,
                            Price=?Price?,
                            ProductName=?ProductName?,                          
                            Vendor=?Vendor?
                        where ID=?ID?;";
            using (IDbConnection con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                return (await con.ExecuteAsync(sql, product));
            }
        }

        public async Task<int> RemoveProduct(int id)
        {
            string sql = @"Delete from Product where Id=@Id";
            using (IDbConnection con = new OleDbConnection(new Connection().BuildConnectionString))
            {
                return (await con.ExecuteAsync(sql, new { Id=id}));
            }
        }
    }
}
