﻿Imports System.Globalization
Imports System.Runtime.CompilerServices

Module Currency
    <Extension()>
    Function ToDecimal(ByVal currency As String) As Decimal
        Return Decimal.Parse(currency, NumberStyles.Currency)
    End Function
End Module
