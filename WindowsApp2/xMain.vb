﻿Imports DevExpress.XtraNavBar

Public Class xMain

    Shared Sub New()
        
    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        OpenProduct()
    End Sub

    Private _product As xProduct

    Private Sub NavBarItem1_LinkClicked(sender As Object, e As NavBarLinkEventArgs) Handles NavBarItem1.LinkClicked
        OpenProduct()
    End Sub

    Sub OpenProduct()
  If (_product IsNot Nothing) Then
            _product.Activate()
        Else
            _product = New xProduct() With
                {
                .MdiParent = Me
                }
            AddHandler Me._product.Closed, New EventHandler(Sub(o As Object, n As EventArgs) Me._product = Nothing)
            _product.Show()
        End if
    End Sub
End Class
