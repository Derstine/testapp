﻿
Imports Autofac

Public Class Resolver(Of T)
        Public ReadOnly Property Resolve As T
            Get
                Dim product As T

                Using scope = Register.Container.BeginLifetimeScope()
                    product = scope.Resolve(Of T)()
                End Using

                Return product
            End Get
        End Property
    End Class

