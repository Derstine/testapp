﻿Imports DevExpress.XtraBars
Imports DevExpress.XtraBars.Docking
Imports DevExpress.XtraGrid.Views.Grid
Imports Inventory.Component
Imports Inventory.Model
Imports Product

Public Class xProduct
    Private ReadOnly product As InventoryComponent = New InventoryComponent(New Resolver(Of IProduct)().Resolve)

    Private Sub BarStaticItemAdd_ItemClick(sender As Object, e As ItemClickEventArgs) Handles BarStaticItemAdd.ItemClick

        DockPanel1.Visibility = DockVisibility.Visible
        DockPanel1.Text = "Add New Product"
        TextEditProductName.Text = String.Empty
        TextEditCategory.Text = String.Empty
        TextEditBarcode.Text = String.Empty
        TextEditVendor.Text = String.Empty
        TextEditPrice.Text = String.Empty
    End Sub

    Private Sub BarStaticItem3_ItemClick(sender As Object, e As ItemClickEventArgs) Handles BarStaticItem3.ItemClick
        DockPanel1.Visibility = DockVisibility.Visible
        DockPanel1.Text = "Edit Product"
    End Sub

    Private Async Sub xProduct_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        Load()
    End Sub

    Dim row As ProductModel

    Private Sub GridView1_RowClick(sender As Object, e As RowClickEventArgs) Handles GridView1.RowClick
        row = TryCast(Me.gridView1.GetRow(Me.gridView1.FocusedRowHandle), ProductModel)
        If row IsNot Nothing
            TextEditProductName.Text = row.ProductName
            TextEditCategory.Text = row.Category
            TextEditBarcode.Text = row.Barcode
            TextEditVendor.Text = row.Vendor
            TextEditPrice.Text = row.Price
        End If
    End Sub

    Private async Sub SimpleButtonAddProduct_Click(sender As Object, e As EventArgs) Handles SimpleButtonSave.Click

        Dim productModel as New ProductModel
        productModel.ProductName = TextEditProductName.Text
        productModel.Category = TextEditCategory.Text
        productModel.Barcode = TextEditBarcode.Text
        productModel.Vendor = TextEditVendor.Text
        productModel.Price = TextEditPrice.Text.ToDecimal()

        If DockPanel1.Text.Contains("Add") Then
            Await product.CreateNewProduct(productModel)
        Else
            productModel.ID = row.ID
            Await product.UpdateProduct(productModel)
        End If
        Load()
        DockPanel1.Visibility = DockVisibility.Hidden
    End Sub

    async sub Load()
        GridControl1.DataSource = Await product.GetAllProduct()
    End sub

    Private Async Sub BarStaticItemRemove_ItemClick(sender As Object, e As ItemClickEventArgs) _
        Handles BarStaticItemRemove.ItemClick
        row = TryCast(Me.gridView1.GetRow(Me.gridView1.FocusedRowHandle), ProductModel)
        If row.ID > 0 Then
            Await product.Remove(row.ID)
            Load()
        End If
    End Sub
End Class