﻿Imports System.ComponentModel
Imports Autofac
Imports Autofac.Builder
Imports Inventory.Data.vb


Public Module Register
    Public Property Container As Autofac.IContainer
    Public Sub Ioc()
        Dim builder = New Autofac.ContainerBuilder()
        builder.RegisterType(Of Product)().[As](Of Inventory.Component.IProduct)()
        Container  = builder.Build(ContainerBuildOptions.None)
    End Sub
End Module
