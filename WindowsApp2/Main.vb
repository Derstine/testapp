﻿Imports DevExpress.Skins

Friend  Module Program
    <STAThread>
    Public Sub Main()

        SkinManager.EnableFormSkins()
        Application.EnableVisualStyles()
        Application.SetCompatibleTextRenderingDefault(False)
        Register.Ioc()
        Application.Run(New xMain)
    End Sub
End Module