﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Inventory.Model;

namespace Product
{
    public class ProductComponent
    {
        private readonly IProduct _product;

        public ProductComponent(IProduct product)
        {
            _product = product;
        }

        public async Task<List<ProductModel>> GetAllProduct()
        {
            return await _product.GetAllProduct();
        }

        public async Task<int> CreateNewProduct(ProductModel model)
        {
            
            return await _product.AddNewProduct(model);
        }

        public async Task<int> UpdateProduct(ProductModel model)
        {
            return await _product.UpdateProduct(model);
        }

        public async Task<int> Remove(int productId)
        {
            return await _product.RemoveProduct(productId);
        }


    }

    public interface IProduct
    {
        Task<List<ProductModel>> GetAllProduct();
        Task<int> AddNewProduct(ProductModel product);
        Task<int> UpdateProduct(ProductModel product);
        Task<int> RemoveProduct(int id);
    }

}
