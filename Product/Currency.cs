﻿using System.Globalization;

namespace Product
{

        public static class Currency
        {
            public static decimal ToDecimal(this string currency) => decimal.Parse(currency, NumberStyles.Currency);
        }
    
}