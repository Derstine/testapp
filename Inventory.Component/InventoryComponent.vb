﻿Imports Inventory.Model

Public Class InventoryComponent
    Private ReadOnly _product As IProduct

    Public Sub New(ByVal product As IProduct)
        MyBase.New()
        Me._product = product
    End Sub

    Public Async Function CreateNewProduct(ByVal model As ProductModel) As Task(Of Integer)
        Return Await Me._product.AddNewProduct(model)
    End Function

    Public Async Function GetAllProduct() As Task(Of List(Of ProductModel))
        Return Await Me._product.GetAllProduct()
    End Function

    Public Async Function Remove(ByVal productId As Integer) As Task(Of Integer)
        Return Await Me._product.RemoveProduct(productId)
    End Function

    Public Async Function UpdateProduct(ByVal model As ProductModel) As Task(Of Integer)
        Return Await Me._product.UpdateProduct(model)
    End Function
End Class