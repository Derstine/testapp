﻿Imports Inventory.Model

Public Interface IProduct
    Function AddNewProduct(ByVal product As ProductModel) As Task(Of Integer)

    Function GetAllProduct() As Task(Of List(Of ProductModel))

    Function RemoveProduct(ByVal id As Integer) As Task(Of Integer)

    Function UpdateProduct(ByVal product As ProductModel) As Task(Of Integer)
End Interface