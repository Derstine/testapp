﻿Public Class ProductModel
    
    Public Property ID As Integer
    Public Property ProductName As String
    Public Property Category As String
    Public Property Vendor As String
    Public Property Price As Decimal
    Public Property Barcode As String
End Class
